#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <netdb.h>
#include <unistd.h>
#include <arpa/inet.h>


#define PORT 4242 // Definição da porta de comunicação com o servidor
#define LEN 255 // Tamnho do buffer de mensagem
#define SERVER_HOST "127.0.0.1" //Endereço do servidor


/* Função de início do programa */
int main(int argc, char *argv[]) {

    int sock; // socket local
    struct sockaddr_in server_addr; // socket do servidor

    char buffer[LEN]; // buffer de mensagem

    printf("Iniciando Cliente...\n");

    // Definindo propriedades de conexão
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = inet_addr(SERVER_HOST);
    server_addr.sin_port = htons(PORT);

    // Criando socket cliente
    if ((sock = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Erro ao criar socket cliente.");
        return EXIT_FAILURE;
    }

    // Conectando-se ao servidor
    if (connect(sock, (struct sockaddr*) &server_addr, sizeof(server_addr)) == -1) {
        perror("Erro ao se comunicar com o servidor.");
        return EXIT_FAILURE;
    }

    // Enviando mensagem para o servidor através do socket
    write(sock, argv[1], 1 + strlen(argv[1]));

    // recebendo mensagem do servidor através do socket
	read(sock, buffer, sizeof(buffer));

    printf("Texto enviado: %s\n", argv[1]);
	printf("Resultado: %s\n", buffer);

    printf("\nFechando conexão.\n");
    // Fechando a conexão
    close(sock);

    return EXIT_SUCCESS;
}





