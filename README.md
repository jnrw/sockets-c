# Cifra de César - Sockets

Exemplo de utilização de sockets e processos em C.

# Compilação

```sh
gcc server.c -o server
gcc client.c -o client
```

# Utilização

**Criptografar Mensagem**

```sh
$ ./server
$ ./client "c|a=e;b=f;c=g;d=h;e=i;f=j;g=k;h=l;i=m;j=n;k=o;l=p;m=q;n=r;o=s;p=t;q=u;r=v;s=w;t=x;u=y;v=z;w=a;x=b;y=c;z=d|Aqui tem uma mensagem de exemplo."
```

Resultado:

```
Auym xiq yqe qirwekiq hi ibiqtps.
```

**Descriptografar Mensagem**

```sh
$ ./server
$ ./client "d|a=e;b=f;c=g;d=h;e=i;f=j;g=k;h=l;i=m;j=n;k=o;l=p;m=q;n=r;o=s;p=t;q=u;r=v;s=w;t=x;u=y;v=z;w=a;x=b;y=c;z=d|Auym xiq yqe qirwekiq hi ibiqtps."
```

Resultado:

```
Aqui tem uma mensagem de exemplo.
```
