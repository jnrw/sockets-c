
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>


#define PORT 4242 // Definição da porta de comunicação com o servidor
#define BUFFER_LENGTH 4096 // Tamnho do buffer de mensagem

typedef struct {
    char *texto;
    char *chave;
    char *valor;
    float n_cifras;
    char operacao;
} CifraDeCesar;

int count_char(char *str, char ch);

void carregar_cifras(CifraDeCesar *cifra_de_cesar, char *cifras);

char* criptografar(CifraDeCesar *cifra_de_cesar);

char* descriptografar(CifraDeCesar *cifra_de_cesar);

void processo_cliente(int sock_client);

/* Função de início do programa */
int main(int argc, char *argv[]) {

    struct sockaddr_in client_addr; // informações do socket do cliente
    struct sockaddr_in server_addr; // informações do socket do servidor
    int sock_server; // socket servidor
    int sock_client; // socket cliente

    printf("Iniciando Servidor...\n");

    // Definindo propriedades de conexão do servidor
    memset(&server_addr, 0, sizeof(server_addr));
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons(PORT);

    // Criando socket server
    if ((sock_server = socket(AF_INET, SOCK_STREAM, 0)) == -1) {
        perror("Erro ao criar socket servidor.");
        return EXIT_FAILURE;
    }

    // Abrir socket para receber comunicação
    if(bind(sock_server, (struct sockaddr*)&server_addr, sizeof(server_addr)) == -1 ) {
        perror("Erro ao criar bind.");
        return EXIT_FAILURE;
    }

    // Escutar mensagens vindas pelo socket
    if(listen(sock_server, 1) == -1) {
        perror("Erro ao ouvir mensagens do socket.");
        return EXIT_FAILURE;
    }

    fprintf(stdout, "Ouvindo na porta %d\n", PORT);

    int t_client_addr;
    while(1) {
        // Tenta aceitar quando houver conexão
        t_client_addr = sizeof(client_addr);
        if ((sock_client=accept(sock_server, (struct sockaddr *) &client_addr, &t_client_addr)) == -1) {
            perror("Falha ao aceitar conexão.");
            return EXIT_FAILURE;
        }

        if (sock_client > 0) {
            if (fork() == 0)
			{
				close(sock_server);
				processo_cliente(sock_client);
				exit(0);
			}
			close(sock_client);
        }
    }

    return EXIT_SUCCESS;
}

void processo_cliente(int sock_client)
{
    char buffer_in[BUFFER_LENGTH]; // buffer de envio de mensagens
    char buffer_out[BUFFER_LENGTH]; // buffer de recebimento de mensagens
	// Aguarda por mensagens
    ssize_t er = 0;
	while ((er = read(sock_client, buffer_in, sizeof(buffer_in))) > 0)
	{
		printf("Mensagem recebida: %s\n", buffer_in);

        char *dem = "|";
        char *operacao = strtok(buffer_in, dem);
        char *cifras = strtok(NULL, dem);
        char *texto = strtok(NULL, dem);
        CifraDeCesar* cifra_de_cesar = (CifraDeCesar*) malloc(sizeof(CifraDeCesar));
        cifra_de_cesar->operacao = operacao[0];
        cifra_de_cesar->texto = texto;
        carregar_cifras(cifra_de_cesar, cifras);

        char *resultado;
        if (cifra_de_cesar->operacao == 'c') {
            resultado = criptografar(cifra_de_cesar);
            sprintf(buffer_out, "Mensagem criptografada: [%s]", resultado);
        }

        if (cifra_de_cesar->operacao == 'd') {
            resultado = descriptografar(cifra_de_cesar);
            sprintf(buffer_out, "Mensagem descriptografada: [%s]", resultado);
        }

		write(sock_client, buffer_out, sizeof(buffer_out));
		printf("Enviando: %s\n", buffer_out);

	}
    close(sock_client); // Fechando a conexão com socket do cliente
    printf("Conexão encerrada\n");
}

int count_char(char *str, char ch)
{
    int count = 0;
    for (int i = 0; str[i] != '\0'; ++i)
    {
        char c = str[i];
        if (c == ch)
        {
            count++;
        }
    }
    return count;
}

void carregar_cifras(CifraDeCesar *cifra_de_cesar, char *cifras) {
    int n_cifras = count_char(cifras, ';') + 1;
    cifra_de_cesar->n_cifras = n_cifras;
    cifra_de_cesar->chave = (char*) malloc(sizeof(char) * n_cifras);
    cifra_de_cesar->valor = (char*) malloc(sizeof(char) * n_cifras);

    char *cifra = strtok(cifras, ";");
    for (int i = 0; i < n_cifras; i++) {
        cifra_de_cesar->chave[i] = cifra[0];
        cifra_de_cesar->valor[i] = cifra[2];
        cifra = strtok(NULL, ";");
    }
}

char* criptografar(CifraDeCesar *cifra_de_cesar) {
    char *resultado = (char*) malloc(sizeof(char) * strlen(cifra_de_cesar->texto));
    for (int i = 0; cifra_de_cesar->texto[i] != '\0'; ++i)
    {
        char c = cifra_de_cesar->texto[i];
        resultado[i] = c;
        for (int j = 0; j < cifra_de_cesar->n_cifras; j++) {
            if (cifra_de_cesar->chave[j] == c) {
                resultado[i] = cifra_de_cesar->valor[j];
            }
        }
    }
    return resultado;
}

char* descriptografar(CifraDeCesar *cifra_de_cesar) {
    char *resultado = (char*) malloc(sizeof(char) * strlen(cifra_de_cesar->texto));
    for (int i = 0; cifra_de_cesar->texto[i] != '\0'; ++i)
    {
        char c = cifra_de_cesar->texto[i];
        resultado[i] = c;
        for (int j = 0; j < cifra_de_cesar->n_cifras; j++) {
            if (cifra_de_cesar->valor[j] == c) {
                resultado[i] = cifra_de_cesar->chave[j];
            }
        }
    }
    return resultado;
}


